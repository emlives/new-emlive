import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
  },
  loginBtn: {
    backgroundColor: '#34495e',
    alignItems: 'center',
    textAlignVertical: 'center',
    paddingVertical: 5,
    borderRadius: 2,
    marginHorizontal: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButton: {
    color: 'white',
    fontSize: 25,
  },
  input: {
    color: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginVertical: 20,
    marginHorizontal: 25,
    fontSize: 23,
    fontWeight: '600',
    display: 'none'
  },
  bgImage: {
    flex: 1,
    justifyContent: 'space-around',
  },
});

export default styles;
