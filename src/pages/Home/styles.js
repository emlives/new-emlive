import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3498db',
  },
  liveStreamButton: {
    backgroundColor: 'green',
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 1,
    marginHorizontal: 25,
    marginBottom: 15,
  },
  textButton: {
    color: 'white',
    fontSize: 17,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginVertical: 20,
    marginHorizontal: 25,
    fontSize: 23,
    fontWeight: '600',
  },
  flatList: {
    marginHorizontal: 15,
  },
  welcomeText: {
    fontSize: 15,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 20,
    marginTop: 25,
  },
  title: {
    fontSize: 17,
    color: 'white',
    fontWeight: '300',
    marginLeft: 20,
    marginVertical: 5,
  },
  bgImage: {
    flex: 1,
    justifyContent: 'space-around',
  },
});

export default styles;
