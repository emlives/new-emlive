import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  TextInput, Alert 
} from 'react-native';

import { fonts, colors } from '../../styles';
import { Text } from '../../components/StyledText';

export default function AuthScreen ( props ) {
  const [userName, setuserName] = useState('anchor_1');

  const onPressLogin = () => {
      if (userName === '') {
        Alert.alert('Please input userName'); 
      } else {
        const {
          navigation: { navigate },
        } = props;
        navigate('EMLive Streaming', { userName: userName, roomName: userName });

        
      }  
  };

  return (
    <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Please type any name"
          placeholderTextColor="gray"
          defaultValue={userName}
          onChangeText={newUsername => setuserName(newUsername)}
          autoCorrect={false}
        />
        <TouchableOpacity style={styles.loginBtn} onPress={onPressLogin}>
          <Text style={styles.textButton}>Login</Text>
        </TouchableOpacity>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  bgImage: {
    flex: 1,
    marginHorizontal: -20,
  },
  section: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionLarge: {
    flex: 2,
    justifyContent: 'space-around',
  },
  sectionHeader: {
    marginBottom: 8,
  },
  priceContainer: {
    alignItems: 'center',
  },
  description: {
    padding: 15,
    lineHeight: 25,
  },
  titleDescription: {
    color: '#19e7f7',
    textAlign: 'center',
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  title: {
    marginTop: 30,
  },
  price: {
    marginBottom: 5,
  },
  priceLink: {
    borderBottomWidth: 1,
    borderBottomColor: colors.primary,
  },
  loginBtn: {
    backgroundColor: '#34495e',
    alignItems: 'center',
    paddingVertical: 15,
    borderRadius: 10,
    marginHorizontal: 25,
  },
  textButton: {
    color: 'white',
    fontSize: 25,
  },
  input: {
    color: 'black',
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginVertical: 20,
    marginHorizontal: 25,
    fontSize: 23,
    fontWeight: '600',
    display: 'none'
  },
});
