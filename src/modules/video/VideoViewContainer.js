import { compose, withState } from 'recompose';

import VideoScreen from './VideoView';

export default compose(withState('isExtended', 'setIsExtended', false))(
  VideoScreen,
);
