// @flow
import { compose } from 'recompose';

import StarterPageView from './StarterPageView';

export default compose()(StarterPageView);
