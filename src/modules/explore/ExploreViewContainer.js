import { compose, withState } from 'recompose';

import ExploreScreen from './ExploreView';

export default compose(withState('isExtended', 'setIsExtended', false))(
  ExploreScreen,
);
