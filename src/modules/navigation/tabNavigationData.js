import Home from '../../pages/Home'; //'../home';
import ExploreScreen from '../explore/ExploreViewContainer';
//import VideoScreen from '../video/VideoViewContainer';
import FavoritesScreen from '../favorites/FavoritesViewContainer';
import ProfileScreen from '../profile/ProfileViewContainer';

import VideoScreen from '../../pages/Streamer'; 

//import CalendarScreen from '../calendar/CalendarViewContainer';
//import GridsScreen from '../grids/GridsViewContainer';
//import PagesScreen from '../pages/PagesViewContainer';
//import ComponentsScreen from '../components/ComponentsViewContainer';

const iconHome = require('../../../assets/images/tabbar/home.png');
const iconCalendar = require('../../../assets/images/tabbar/calendar.png');
const iconGrids = require('../../../assets/images/tabbar/grids.png');
const iconPages = require('../../../assets/images/tabbar/pages.png');
const iconComponents = require('../../../assets/images/tabbar/components.png');

const iconExplore = require('../../../assets/images/tabbar/explore.png');
const iconVideo = require('../../../assets/images/tabbar/video.png');
const iconFavorites = require('../../../assets/images/tabbar/favorites.png');
const iconProfile = require('../../../assets/images/tabbar/profile.png');

const tabNavigationData = [
  {
    name: 'Home',
    component: Home,
    icon: iconHome,
  },
  {
    name: 'Explore',
    component: ExploreScreen,
    icon: iconExplore,
  },
  {
    name: 'Video',
    component: VideoScreen,
    icon: iconVideo,
  },
  {
    name: 'Favs',
    component: FavoritesScreen,
    icon: iconFavorites,
  },
  {
    name: 'Profile',
    component: ProfileScreen,
    icon: iconProfile,
  },
];

export default tabNavigationData;