import { compose, withState } from 'recompose';

import FavoritesScreen from './FavoritesView';

export default compose(withState('isExtended', 'setIsExtended', false))(
  FavoritesScreen,
);
